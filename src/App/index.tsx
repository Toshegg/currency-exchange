import React, { FunctionComponent } from 'react';
import { StyledApp } from './styles';
import ExchangePage from '../components/organisms/ExchangePage';

const App: FunctionComponent = () => {
  return (
    <StyledApp>
      <ExchangePage />
    </StyledApp>
  );
};

export default App;
