import { GLOBAL_REDUCER_NAMESPACE } from '../util/constants';
import { GlobalAction, Pocket } from '../types';
import { namespacer } from '../util/helpers/namespacer';

const namespacedAction = namespacer(GLOBAL_REDUCER_NAMESPACE);

export const actionTypes = {
  ADD_MONEY: {
    GET: namespacedAction('ADD_MONEY_GET'),
    SUCCESS: namespacedAction('ADD_MONEY_SUCCESS'),
    FAILURE: namespacedAction('ADD_MONEY_FAILURE'),
  },
};

export const actions = {
  addMoney(pocketId: number, value: number): GlobalAction {
    return {
      payload: { pocketId, value },
      type: actionTypes.ADD_MONEY.GET,
    };
  },
  addMoneySuccess(pockets: Pocket[]): GlobalAction {
    return {
      payload: { pockets },
      type: actionTypes.ADD_MONEY.SUCCESS,
    };
  },
  addMoneyFailure(error: boolean): GlobalAction {
    return {
      payload: {
        error,
      },
      type: actionTypes.ADD_MONEY.FAILURE,
    };
  },
};
