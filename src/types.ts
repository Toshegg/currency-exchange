import { ExchangeRateState } from './components/organisms/ExchangePage/types';

export interface AppState {
  exchangeRate: ExchangeRateState;
  global: GlobalState;
}

export interface GlobalState {
  error?: boolean;
  pockets: Pocket[];
}

export interface GlobalAction {
  payload: GlobalPayloads;
  type: string;
}

export interface GlobalPayloads {
  error?: boolean;
  value?: number;
  pocketId?: number;
  pockets?: Pocket[];
}

export interface Pocket {
  id: number;
  currency: string;
  symbol: string;
  balance: number;
}
