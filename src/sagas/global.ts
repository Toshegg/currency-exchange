import { actionTypes, actions } from '../actions';
import { all, put, takeLatest, select } from 'redux-saga/effects';
import { GlobalAction, Pocket, AppState } from '../types';

export const getPockets = (state: AppState) => {
  if (!state) {
    return [];
  }

  return state.global.pockets;
};

export const addValue = (
  pockets: Pocket[],
  id: number | undefined,
  value: number | undefined,
): Pocket[] => {
  const pocket = pockets.find(_pocket => _pocket.id === id);

  if (!pocket || !value) {
    return pockets;
  }

  return [
    ...pockets.slice(0, pockets.indexOf(pocket)),
    { ...pocket, balance: pocket.balance + value },
    ...pockets.slice(pockets.indexOf(pocket) + 1, pockets.length),
  ];
};

export function* addMoney(action: GlobalAction) {
  try {
    const pockets = yield select(getPockets);

    const newPockets = addValue(
      pockets,
      action.payload.pocketId,
      action.payload.value,
    );

    yield put(actions.addMoneySuccess(newPockets));
  } catch (error) {
    yield put(actions.addMoneyFailure(error));
  }
}

export default function* globalRoot() {
  yield all([takeLatest<GlobalAction>(actionTypes.ADD_MONEY.GET, addMoney)]);
}
