import { all } from 'redux-saga/effects';

import exchangeRateRoot from '../components/organisms/ExchangePage/sagas';
import globalRoot from './global';

export default function* rootSaga() {
  yield all([exchangeRateRoot(), globalRoot()]);
}
