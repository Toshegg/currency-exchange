import { expectSaga } from 'redux-saga-test-plan';
import * as matchers from 'redux-saga-test-plan/matchers';
import { throwError } from 'redux-saga-test-plan/providers';
import { all, takeLatest } from 'redux-saga/effects';

import { actionTypes, actions } from '../../actions';
import globalRoot, { addValue, addMoney, getPockets } from '../global';
import { Pocket } from '../../types';

describe('Global | Sagas', () => {
  describe('addValue()', () => {
    it('should correctly add value to the pocket', () => {
      const samplePockets: Pocket[] = [
        {
          id: 0,
          currency: 'EUR',
          symbol: '$',
          balance: 32,
        },
        {
          id: 1,
          currency: 'EUR',
          symbol: '$',
          balance: 24,
        },
        {
          id: 2,
          currency: 'EUR',
          symbol: '$',
          balance: 151,
        },
      ];

      let expected: Pocket[] = [
        samplePockets[0],
        {
          id: 1,
          currency: 'EUR',
          symbol: '$',
          balance: 26,
        },
        samplePockets[2],
      ];

      expect(addValue(samplePockets, 1, 2)).toEqual(expected);

      expected = [
        samplePockets[0],
        samplePockets[1],
        {
          id: 2,
          currency: 'EUR',
          symbol: '$',
          balance: 149,
        },
      ];

      expect(addValue(samplePockets, 2, -2)).toEqual(expected);
    });
  });

  describe('addMoney()', () => {
    it('calls success action if request returns data', () => {
      return expectSaga(addMoney, {
        type: '',
        payload: { pocketId: 1, value: 3 },
      })
        .provide([[matchers.call.fn(addValue), []]])
        .put(actions.addMoneySuccess([]))
        .run();
    });
  });

  describe('globalRoot()', () => {
    it('combines all global sagas', () => {
      const saga = globalRoot();

      expect(saga.next().value).toEqual(
        all([takeLatest(actionTypes.ADD_MONEY.GET, addMoney)]),
      );
    });
  });
});
