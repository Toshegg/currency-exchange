import api from '../api';
import { APP_ID } from '../../util/constants';

describe('api()', () => {
  beforeAll(() => {
    window.fetch = jest.fn();
  });

  describe('getExchangeRateData()', () => {
    it('should call a fetchData function', done => {
      api.getExchangeRateData();

      expect(window.fetch).toHaveBeenCalledWith(
        `https://openexchangerates.org/api/latest.json?app_id=${APP_ID}`,
      );

      done();
    });
  });
});
