import { ExchangeRateData } from '../components/organisms/ExchangePage/types';
import { APP_ID } from '../util/constants';

export const URLs = {
  getExchangeRateData: (appId: string): string =>
    `https://openexchangerates.org/api/latest.json?app_id=${appId}`,
};

export default {
  async getExchangeRateData(): Promise<ExchangeRateData> {
    const response = await fetch(URLs.getExchangeRateData(APP_ID));
    return await response.json();
  },
};
