import { AppState } from '../../../types';
import { EXCHANGE_REDUCER_NAMESPACE } from '../../../util/constants';
import { ExchangeRateState } from './types';

export const exchangeRateSelector = (state: AppState): ExchangeRateState => {
  return state[EXCHANGE_REDUCER_NAMESPACE];
};
