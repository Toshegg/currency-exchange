import '@testing-library/jest-dom/extend-expect';

import { render } from '@testing-library/react';
import { waitForDomChange } from '@testing-library/dom';
import React from 'react';
import ExchangePage from '..';
import createStore from '../../../../store';
import { Provider } from 'react-redux';
import { RATES } from '../../../../util/constants';

const renderComponent = ({ store = createStore() } = {}) => {
  return render(
    <Provider store={store}>
      <ExchangePage />
    </Provider>,
  );
};

beforeAll(() => {
  window.fetch = jest.fn(async () => {
    return await Promise.resolve({
      json: () => Promise.resolve(RATES),
    });
  });
});

test('exchange button should be disabled by default', () => {
  const { getByText } = renderComponent();

  expect(getByText('Exchange')).toBeInTheDocument();
  expect(getByText('Exchange')).toBeDisabled();
});
