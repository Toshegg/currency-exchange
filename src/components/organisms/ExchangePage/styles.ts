import { Button, IconButton } from '../../atoms/Button/styles';
import styled from 'styled-components';
import ExchangeRate from '../../atoms/ExchangeRate';
import ExchangeBlock from '../../molecules/ExchangeBlock';

export const StyledButton = styled(Button)`
  display: block;
  margin-top: 2rem;
  width: 80%;
`;

export const Wrapper = styled.div`
  max-width: 800px;
  width: 100%;
  margin: 0 auto;
  height: 100%;
  display: flex;
  flex-direction: column;
`;

export const GreyWrapper = styled.div`
  display: flex;
  padding: 2rem;
  padding-top: 3rem;
  flex-direction: column;
  align-items: center;
  position: relative;
  flex-grow: 1;

  &:after {
    content: '';
    position: absolute;
    width: 100vw;
    top: 0;
    bottom: 0;
    background-color: rgb(var(--grey));
    z-index: -1;
  }
`;

export const WhiteWrapper = styled.div`
  position: relative;
  padding: 2rem;
  padding-bottom: 3rem;
`;

export const StyledExchangeRate = styled(ExchangeRate)`
  position: absolute;
  left: 50%;
  bottom: 0;
  z-index: 1;
  transform: translate(-50%, 50%);
`;

export const StyledExchangeBlock = styled(ExchangeBlock)`
  width: 100%;
`;

export const SwitchButton = styled(IconButton)`
  position: absolute;
  bottom: 0;
  transform: translateY(50%);
  z-index: 1;
`;
