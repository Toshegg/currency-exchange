import { Pocket } from '../../../types';

export interface Props {
  rates?: ExchangeRateData;
  getExchangeRate: () => void;
  pockets: Pocket[];
  addMoney: (pocketId: number, value: number) => void;
  exchangeRate?: ExchangeRateData;
}

export interface ExchangeRateAction {
  payload: ExchangeRatePayloads;
  type: string;
}

export interface ExchangeRatePayloads {
  error?: boolean;
  rates?: ExchangeRateData;
}

export interface ExchangeRateState {
  error?: boolean;
  rates?: ExchangeRateData;
}

export interface ExchangeRateData {
  disclaimer: string;
  license: string;
  timestamp: number;
  base: string;
  rates: { [key: string]: number };
}
