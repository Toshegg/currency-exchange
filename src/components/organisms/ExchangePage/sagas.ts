import { all, call, put, takeLatest } from 'redux-saga/effects';

import api from '../../../services/api';

import { actions, actionTypes } from './actions';
import { ExchangeRateAction } from './types';

export function* getExchangeRateData() {
  try {
    const exchangeRateDataResponse = yield call(api.getExchangeRateData);

    yield put(actions.getExchangeRateSuccess(exchangeRateDataResponse));
  } catch (error) {
    yield put(actions.getExchangeRateFailure(error));
  }
}

export default function* exchangeRateRoot() {
  yield all([
    takeLatest<ExchangeRateAction>(
      actionTypes.EXCHANGE_RATES.GET,
      getExchangeRateData,
    ),
  ]);
}
