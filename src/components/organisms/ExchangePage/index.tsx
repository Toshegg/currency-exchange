import React, {
  FunctionComponent,
  useState,
  ChangeEvent,
  SetStateAction,
  Dispatch as ReactDispatch,
  useEffect,
} from 'react';
import { Dispatch as ReduxDispatch } from 'redux';
import {
  StyledButton,
  Wrapper,
  GreyWrapper,
  WhiteWrapper,
  StyledExchangeBlock,
  SwitchButton,
  StyledExchangeRate,
} from './styles';
import ExchangeBlock from '../../molecules/ExchangeBlock';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowsAltV } from '@fortawesome/free-solid-svg-icons';
import { CurrencyHelper } from '../../../util/helpers/currency';
import { Option } from 'react-dropdown';
import { Props } from './types';
import { connect } from 'react-redux';
import { exchangeRateSelector } from './selectors';
import { actions as exchangeRateActions } from './actions';
import { actions as globalActions } from '../../../actions';
import { AppState, Pocket } from '../../../types';
import { globalSelector } from '../../../selectors';

const ExchangePage: FunctionComponent<Props> = (props: Props) => {
  const { getExchangeRate, exchangeRate, pockets, addMoney } = props;

  const [fromPocket, setFromPocket] = useState(pockets[0]);
  const [toPocket, setToPocket] = useState(pockets[1]);

  const [fromValue, setFromValue] = useState(0);
  const [toValue, setToValue] = useState(0);
  const [rate, setRate] = useState(0);

  useEffect(() => {
    getExchangeRate();

    const interval = setInterval(() => {
      getExchangeRate();
    }, 10000);

    return () => {
      clearInterval(interval);
    };
  }, []);

  useEffect(() => {
    setFromPocket(pockets[0]);
    setToPocket(pockets[1]);
  }, [pockets]);

  const updateValues = (_fromValue: number, _toValue: number): void => {
    setFromValue(-Math.abs(_fromValue));
    setToValue(Math.abs(_toValue));
  };

  useEffect(() => {
    if (!exchangeRate) {
      return;
    }

    setRate(
      exchangeRate.rates[toPocket.currency] /
        exchangeRate.rates[fromPocket.currency],
    );
  }, [exchangeRate, fromPocket, toPocket]);

  useEffect(() => {
    updateValues(fromValue, fromValue * rate);
  }, [fromPocket, toPocket, rate]);

  const handleFromValueChange = (
    event: ChangeEvent<HTMLInputElement>,
  ): void => {
    const value = event.target.value;

    if (!value) {
      return;
    }

    updateValues(
      parseFloat(parseFloat(value).toFixed(2)),
      parseFloat((parseFloat(value) * rate).toFixed(2)),
    );
  };

  const handleToValueChange = (event: ChangeEvent<HTMLInputElement>): void => {
    const value = event.target.value;

    if (!value) {
      return;
    }

    updateValues(
      parseFloat((parseFloat(value) / rate).toFixed(2)),
      parseFloat(parseFloat(value).toFixed(2)),
    );
  };

  const handleSelectedCurrencyChange = (
    value: Option,
    pocketSetter: ReactDispatch<SetStateAction<Pocket>>,
  ): void => {
    pocketSetter(
      pockets.find(pocket => pocket.currency === value.value) || pockets[0],
    );
  };

  const handleSwitchButtonClick = (): void => {
    setFromPocket(toPocket);
    setToPocket(fromPocket);
  };

  const isExchangeDisabled = (): boolean => {
    return Math.abs(fromValue) > fromPocket.balance || fromValue === 0;
  };

  const handleExchangeClick = (): void => {
    addMoney(fromPocket.id, fromValue);
    addMoney(toPocket.id, toValue);
    updateValues(0, 0);
  };

  return (
    <Wrapper>
      <WhiteWrapper>
        <ExchangeBlock
          currencyOptions={CurrencyHelper.getCodes()}
          onSelectedCurrencyChange={(value: Option): void =>
            handleSelectedCurrencyChange(value, setFromPocket)
          }
          selectedCurrency={fromPocket.currency}
          value={fromValue}
          onValueChange={handleFromValueChange}
          currencySymbol={fromPocket.symbol}
          balance={fromPocket.balance}
        />

        <SwitchButton onClick={handleSwitchButtonClick}>
          <FontAwesomeIcon icon={faArrowsAltV} />
        </SwitchButton>

        <StyledExchangeRate
          rate={rate}
          fromSymbol={fromPocket.symbol}
          toSymbol={toPocket.symbol}
        />
      </WhiteWrapper>

      <GreyWrapper>
        <StyledExchangeBlock
          currencyOptions={CurrencyHelper.getCodes()}
          onSelectedCurrencyChange={(value: Option): void =>
            handleSelectedCurrencyChange(value, setToPocket)
          }
          selectedCurrency={toPocket.currency}
          value={toValue}
          onValueChange={handleToValueChange}
          currencySymbol={toPocket.symbol}
          balance={toPocket.balance}
        />

        <StyledButton
          disabled={isExchangeDisabled()}
          onClick={handleExchangeClick}
        >
          Exchange
        </StyledButton>
      </GreyWrapper>
    </Wrapper>
  );
};

const mapStateToProps = (state: AppState) => {
  return {
    exchangeRate: exchangeRateSelector(state).rates,
    pockets: globalSelector(state).pockets,
  };
};

export const mapDispatchToProps = (dispatch: ReduxDispatch) => {
  return {
    getExchangeRate: () => dispatch(exchangeRateActions.getExchangeRate()),
    addMoney: (pocketId: number, value: number) =>
      dispatch(globalActions.addMoney(pocketId, value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ExchangePage);
