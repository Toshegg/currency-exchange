import { EXCHANGE_REDUCER_NAMESPACE } from '../../../util/constants';
import { ExchangeRateAction, ExchangeRateData } from './types';
import { namespacer } from '../../../util/helpers/namespacer';

const namespacedAction = namespacer(EXCHANGE_REDUCER_NAMESPACE);

export const actionTypes = {
  EXCHANGE_RATES: {
    GET: namespacedAction('EXCHANGE_RATES_GET'),
    FAILURE: namespacedAction('EXCHANGE_RATES_SUCCESS'),
    SUCCESS: namespacedAction('EXCHANGE_RATES_FAILURE'),
  },
};

export const actions = {
  getExchangeRate(): ExchangeRateAction {
    return {
      payload: {},
      type: actionTypes.EXCHANGE_RATES.GET,
    };
  },
  getExchangeRateSuccess(rates: ExchangeRateData): ExchangeRateAction {
    return {
      payload: {
        rates,
      },
      type: actionTypes.EXCHANGE_RATES.SUCCESS,
    };
  },
  getExchangeRateFailure(error: boolean): ExchangeRateAction {
    return {
      payload: {
        error,
      },
      type: actionTypes.EXCHANGE_RATES.FAILURE,
    };
  },
};
