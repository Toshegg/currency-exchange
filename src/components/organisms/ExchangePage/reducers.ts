import { actionTypes } from './actions';
import { ExchangeRateState, ExchangeRateAction } from './types';

export const initialState: ExchangeRateState = {
  error: false,
  rates: undefined,
};

export default (
  state: ExchangeRateState = initialState,
  action: ExchangeRateAction,
): ExchangeRateState => {
  switch (action.type) {
    case actionTypes.EXCHANGE_RATES.GET:
      return {
        ...state,
      };

    case actionTypes.EXCHANGE_RATES.SUCCESS:
      return {
        ...state,
        rates: action.payload.rates,
      };

    case actionTypes.EXCHANGE_RATES.FAILURE:
      return {
        ...state,
        error: action.payload.error,
      };

    default:
      return state;
  }
};
