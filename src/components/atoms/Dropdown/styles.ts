import styled from 'styled-components';
import ReactDropdown from 'react-dropdown';
import 'react-dropdown/style.css';

export const Dropdown = styled(ReactDropdown)`
  &.Dropdown-root {
    font-size: 3rem;
    text-transform: uppercase;
  }

  & .Dropdown-control {
    background: transparent;
    border: 0;
    padding: 0;
    cursor: pointer;
    display: inline-flex;
    align-items: center;

    &:hover {
      box-shadow: none;
    }
  }

  & .Dropdown-arrow-wrapper {
    margin-left: 5px;

    & .Dropdown-arrow {
      top: 0;
      left: 0;
      position: static;
    }
  }

  & .Dropdown-menu {
    border: 0;
    box-shadow: none;
    border-radius: 5px;
  }

  & .Dropdown-option {
    cursor: pointer;
    padding: 1rem 2rem;
  }

  & .Dropdown-option + .Dropdown-option {
    border-top: 1px solid rgb(var(--grey));
  }
`;
