import styled from 'styled-components';

export const Input = styled.input`
  background: transparent;
  border: 0;
  font-size: 3rem;
  text-overflow: ellipsis;

  &[type='number']::-webkit-inner-spin-button,
  &[type='number']::-webkit-outer-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
`;
