export interface Props {
  className?: string;
  fromValue?: number;
  fromSymbol?: string;
  toSymbol?: string;
  rate: number;
}
