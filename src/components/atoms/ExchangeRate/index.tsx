import React, { FunctionComponent, useState, useEffect } from 'react';
import { Wrapper, RateText } from './styles';
import { Props } from './types';

export const getToValue = (fromValue: number, rate: number): number => {
  return fromValue * rate;
};

const ExchangeRate: FunctionComponent<Props> = (props: Props) => {
  const { className, toSymbol, fromValue = 1, fromSymbol, rate } = props;
  const [toValue, setToValue] = useState(getToValue(fromValue, rate));

  useEffect(() => {
    setToValue(getToValue(fromValue, rate));
  }, [rate, fromValue]);

  return (
    <Wrapper className={className}>
      <RateText>
        {`${fromSymbol}${fromValue}`} = {`${toSymbol}${toValue}`}
      </RateText>
    </Wrapper>
  );
};

export default ExchangeRate;
