import styled from 'styled-components';

export const Wrapper = styled.div`
  display: inline-block;
  border: 2px solid rgb(var(--grey));
  padding: 0.5rem 1rem;
  border-radius: 20px;
  background: rgb(var(--white));
`;

export const RateText = styled.div`
  white-space: nowrap;
  color: rgb(var(--blue));
`;
