import styled from 'styled-components';

export const Button = styled.button`
  background: rgb(var(--pink));
  color: rgb(var(--white));
  border: 0;
  border-radius: 20px;
  font-size: 2rem;
  padding: 1rem 2rem;
  box-shadow: 5px 5px 5px rgba(var(--pink), 0.2);
  cursor: pointer;
  transition: background 0.2s ease-in-out;

  &:hover,
  &:active {
    background: rgba(var(--pink), 0.8);
  }

  &[disabled] {
    background: rgba(var(--pink), 0.3);
    cursor: default;
  }
`;

export const IconButton = styled.button`
  color: rgb(var(--blue));
  border: 2px solid rgb(var(--grey));
  border-radius: 50%;
  font-size: 1.5rem;
  padding: 0.5rem 1rem;
  background: rgb(var(--white));
  cursor: pointer;
  transition: background 0.2s ease-in-out;

  &:hover,
  &:active {
    background: rgb(var(--grey));
  }
`;
