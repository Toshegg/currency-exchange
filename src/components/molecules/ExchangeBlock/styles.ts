import styled from 'styled-components';
import { Input } from '../../atoms/Input/styles';
import { Dropdown } from '../../atoms/Dropdown/styles';

export const Wrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-gap: 1rem;
`;

export const PriceInput = styled(Input)`
  text-align: right;
  width: 100%;
`;

export const CurrentBalance = styled.div`
  grid-row: 2;
  color: rgb(var(--grey-dark));
`;

export const CurrencyDropdown = styled(Dropdown)``;
