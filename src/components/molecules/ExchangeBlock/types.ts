import { Option } from 'react-dropdown';
import { ChangeEvent } from 'react';

export interface Props {
  className?: string;
  currencyOptions: string[];
  selectedCurrency?: string;
  onSelectedCurrencyChange: (value: Option) => void;
  onValueChange: (event: ChangeEvent<HTMLInputElement>) => void;
  value: number;
  balance: number;
  currencySymbol: string;
}
