import React, { FunctionComponent } from 'react';
import { Props } from './types';
import {
  Wrapper,
  CurrentBalance,
  PriceInput,
  CurrencyDropdown,
} from './styles';

const ExchangeBlock: FunctionComponent<Props> = (props: Props) => {
  const {
    className,
    currencyOptions,
    selectedCurrency,
    onSelectedCurrencyChange,
    onValueChange,
    value,
    balance,
    currencySymbol,
  } = props;

  return (
    <Wrapper className={className}>
      <CurrencyDropdown
        options={currencyOptions}
        value={selectedCurrency}
        onChange={onSelectedCurrencyChange}
      />
      <CurrentBalance>
        Balance: {`${currencySymbol}${balance.toFixed(2)}`}
      </CurrentBalance>
      <PriceInput
        placeholder="0"
        step="0.01"
        title="Currency"
        pattern="^\d+(?:\.\d{1,2})?$"
        type="number"
        onChange={onValueChange}
        value={value}
      />
    </Wrapper>
  );
};

export default ExchangeBlock;
