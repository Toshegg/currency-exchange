import { combineReducers } from 'redux';
import exchangeRateReducer from '../components/organisms/ExchangePage/reducers';
import globalReducer from './global';

export default combineReducers({
  exchangeRate: exchangeRateReducer,
  global: globalReducer,
});
