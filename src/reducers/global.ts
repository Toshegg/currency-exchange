import { GlobalState, GlobalAction } from '../types';
import { AVAILABLE_CURRENCY } from '../util/helpers/currency';
import { actionTypes } from '../actions';

export const initialState: GlobalState = {
  pockets: AVAILABLE_CURRENCY.map((item, index) => ({
    id: index,
    currency: item.code,
    symbol: item.symbol,
    balance: parseFloat((Math.random() * 1000).toFixed(2)),
  })),
};

export default (
  state: GlobalState = initialState,
  action: GlobalAction,
): GlobalState => {
  switch (action.type) {
    case actionTypes.ADD_MONEY.GET:
      return {
        ...state,
      };

    case actionTypes.ADD_MONEY.SUCCESS:
      return {
        ...state,
        pockets: action.payload.pockets || [],
      };

    case actionTypes.ADD_MONEY.FAILURE:
      return {
        ...state,
        error: action.payload.error,
      };

    default:
      return state;
  }
};
