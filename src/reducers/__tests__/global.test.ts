import reducer, { initialState } from '../global';
import { GlobalAction } from '../../types';
import { actionTypes } from '../../actions';

describe('Global | Reducer', () => {
  it('should default to the initial state', () => {
    expect(reducer(undefined, { type: 'test_type', payload: {} })).toBe(
      initialState,
    );
  });

  describe('ADD_MONEY', () => {
    describe('GET', () => {
      it('should get state for ADD_MONEY.GET', () => {
        const action: GlobalAction = {
          payload: {
            pocketId: 1,
            value: 32,
          },
          type: actionTypes.ADD_MONEY.GET,
        };

        expect(reducer(initialState, action)).toEqual(initialState);
      });
    });

    describe('SUCCESS', () => {
      it('should update state for ADD_MONEY.SUCCESS', () => {
        const action: GlobalAction = {
          payload: {
            pockets: [],
          },
          type: actionTypes.ADD_MONEY.SUCCESS,
        };

        expect(reducer(initialState, action).pockets).toEqual([]);
      });
    });

    describe('FAILURE', () => {
      it('should update state for ADD_MONEY.FAILURE', () => {
        const action: GlobalAction = {
          payload: {
            error: true,
          },
          type: actionTypes.ADD_MONEY.FAILURE,
        };

        expect(reducer(initialState, action).error).toEqual(
          action.payload.error,
        );
      });
    });
  });
});
