import { AppState, GlobalState } from '../types';
import { GLOBAL_REDUCER_NAMESPACE } from '../util/constants';

export const globalSelector = (state: AppState): GlobalState => {
  return state[GLOBAL_REDUCER_NAMESPACE];
};
