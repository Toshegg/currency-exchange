import reducer, { initialState } from '../../reducers/global';
import { globalSelector } from '../';
import { AppState } from '../../types';
import { GLOBAL_REDUCER_NAMESPACE } from '../../util/constants';

describe('Global | Selectors', () => {
  describe('globalSelector()', () => {
    it('should accept the state and return pockets', () => {
      const state: AppState = {
        exchangeRate: {},
        [GLOBAL_REDUCER_NAMESPACE]: reducer(
          {
            ...initialState,
          },
          {
            payload: {},
            type: '',
          },
        ),
      };

      expect(globalSelector(state).pockets.length).toEqual(3);
    });
  });
});
