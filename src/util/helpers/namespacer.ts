export const namespacer = (namespace: string) => (action: string): string =>
  `${namespace}/${action}`;
