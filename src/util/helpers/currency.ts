export type Currency = {
  code: string;
  symbol: string;
};

export const AVAILABLE_CURRENCY = [
  {
    code: 'EUR',
    symbol: '€',
  },
  {
    code: 'GBP',
    symbol: '£',
  },
  {
    code: 'USD',
    symbol: '$',
  },
];

export class CurrencyHelper {
  static availableCurrency = AVAILABLE_CURRENCY;

  static findCurrencyByCode(code: string): Currency | undefined {
    return this.availableCurrency.find(cur => cur.code === code);
  }

  static getCodes(): string[] {
    return this.availableCurrency.map(item => item.code);
  }
}
